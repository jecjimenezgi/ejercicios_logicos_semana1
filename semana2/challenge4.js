function landMass(name,area) {
    //Implementación
    const terraMass = 148940000
    percent = ((area/terraMass)*100).toFixed(2)
    return {"percent": Number(percent), "message": `${name} representa el ${percent}% de la masa de la tierra`}

}

module.exports = landMass;


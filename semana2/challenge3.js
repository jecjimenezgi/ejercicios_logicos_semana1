function arrayOfMultiples (num, length) {
    const range = (start, stop, step) =>Array(Math.ceil((stop - start) / step)).fill(start).map((x, y) => x + y * step)
    if (num == 1){
        range(1,length,1)
    }
    return range(num,num*length+1,num);

}

module.exports = arrayOfMultiples;



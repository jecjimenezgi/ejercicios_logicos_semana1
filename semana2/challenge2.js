function divisibleByLeft(n) {
    //Implementación
    let base = n.toString().split('').map(Number);
    return base.map((x,i) => {
        if (i >= 1) {
            if ( base[i] % base[i-1] != 0) {
                return false;
            }else{
                return true
            }
        } else{
            return false
        }
    })
}

module.exports = divisibleByLeft;
//Un arreglo es especial si cada indice par contiene un numero par y cada indice impar contiene un numero impar. Crea una función que regrese true si un arreglo es especial, y de lo contrario false
function isSpecialArray (specialArr) {
  for (let i = 0; i < specialArr.length; i++) {
    if (i % 2 === 0) {
      if (specialArr[i] % 2 !== 0) {
        return false
      }
    } else {
      if (specialArr[i] % 2 === 0) {
        return false
      }
    }
  }
  return true
}

module.exports = isSpecialArray;
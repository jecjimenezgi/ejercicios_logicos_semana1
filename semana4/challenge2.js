//Crea una función que revise si dos objetos son iguales el uno con el otro. Regresa true si los objetos son iguales, de lo contrario regresa false.
function isEqual(objInput1, objInput2) {
    //Implementación
    return JSON.stringify(objInput1) === JSON.stringify(objInput2);
}

module.exports = isEqual;
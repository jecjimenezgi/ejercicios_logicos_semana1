//Crea una función que tome un objeto como argumento y regrese las propiedades y valores en arreglos separados. Regresa las propiedades (keys) ordenadas alfabéticamente y junto con valores en el mismo orden.
function keysAndValues(objInput) {
    //Implementación
    var keys = Object.keys(objInput).sort();
    var values = keys.map((key) => objInput[key]);
    return [keys, values];
}

module.exports = keysAndValues;

const reverseInt = (number) => {
    if (typeof(number) !== 'number' || number.toString().split('').includes('.')) {
        throw new Error('Tipo de dato no admitido.');
    }

    if (number.toString().split('').includes('-')) {
        number = number.toString().split('').slice(1,number.length).join('');
        return parseInt("-" + parseInt(number.toString().split('').reverse().join('')));
    }

    return parseInt(number.toString().split('').reverse().join(''));

}

module.exports = {
    reverseInt
}



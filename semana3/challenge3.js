function mcm (a, b) {
  let mcm = a * b;
  let i = 1;
  while (i <= mcm) {
    if (i % a === 0 && i % b === 0) {
      return i;
    }
    i++;
  }

}

module.exports = mcm;


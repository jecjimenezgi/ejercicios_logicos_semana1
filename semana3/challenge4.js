function toArray(obj) {
  var array = [];
  for (var key in obj) {
    array.push([key, obj[key]]);
  }
  return array;

}

module.exports = toArray;
function minMax(aNumbs) {
  //Implementación
  const max = aNumbs.reduce((a, b) => (a > b ? a : b));
  const min = aNumbs.reduce((a, b) => (a < b ? a : b));
  return { min: min, max: max };
}

module.exports = minMax;

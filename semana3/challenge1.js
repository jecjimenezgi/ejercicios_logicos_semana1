// A function that allow you to count all numbers less than a integer number
function countPrimes(number) {
    //Implementación

    const isPrime = (num) => {
        for (let i = 2; i < num; i++) {
            if (num % i === 0) {
                return false;
            }
        }
        return true;
    }

    let count = 0;
    for (let i = 2; i < number; i++) {
        if (isPrime(i)) {
            count++;
        }
    }
    return count;
}

module.exports = countPrimes;


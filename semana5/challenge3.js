// Crea una función que tome un string y devuelva un arreglo de las letras que aparezcan una sola vez
function findLetters (str) {
  let arr = str.split('');
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    let count = 0;
    for (let j = 0; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        count++;
      }
    }
    if (count === 1) {
      result.push(arr[i]);
    }
  }
  return result;

}

module.exports = findLetters;